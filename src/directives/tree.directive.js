import angular from 'angular';
import HumanTreeNode from '../entities/humanTreeNode';

require('script!sigma');

function greeting($location) {
    return {
        restrict: 'E',
        scope: {
            root: '='
        },
        link: function(scope, element) {
            element[0].style.display = 'block';
            var s = new sigma(element[0]);
            s.bind('clickNode', event => {
                $location.path(`/humans/edit/${event.data.node.id}`);
                scope.$apply();
            });

            scope.$watch('root', function(root) {
                /** @type {?HumanTreeNode} root */

                s.graph.clear();

                if (!(root instanceof HumanTreeNode)) return;

                var g = {
                    nodes: [],
                    edges: []
                };

                var addedNodesCache = [];
                var levelsPositionCache = {};

                /**
                 * @param {HumanTreeNode} human
                 * @param {int} level
                 * TODO: It's is bad idea to use recursion to build tree
                 */
                function addHumanNode(human, level = 0) {
                    if (addedNodesCache.indexOf(human.id) >= 0) return;

                    let size;
                    switch (Math.abs(level)) {
                        case 0: size = 3; break;
                        case 1: size = 2; break;
                        default : size = 1;
                    }

                    if (!levelsPositionCache.hasOwnProperty(`${level}`)) {
                        levelsPositionCache[level] = 0;
                    }

                    let position = levelsPositionCache[level];
                    let x = position % 2 == 0 ? (- position / 2) : ((position - 1) / 2 + 1);
                    levelsPositionCache[level] = position + 1;

                    g.nodes.push({
                        id: human.id,
                        label: human.name,
                        x: level % 2 ? (x - 0.5) : x,
                        y: level,
                        size: size,
                        color: human == root ? '#286090' : '#666'
                    });
                    addedNodesCache.push(human.id);

                    if (human.mother) {
                        // add parents only when we going by ancestors
                        if (level <= 0) addHumanNode(human.mother, level - 1);

                        g.edges.push({
                            id: `${human.id}-${human.mother.id}`,
                            source: human.id,
                            target: human.mother.id,
                            size: 1,
                            color: '#ccc'
                        });
                    }

                    if (human.father) {
                        // add parents only when we going by ancestors
                        if (level <= 0) addHumanNode(human.father, level - 1);

                        g.edges.push({
                            id: `${human.id}-${human.father.id}`,
                            source: human.id,
                            target: human.father.id,
                            size: 1,
                            color: '#ccc'
                        });
                    }

                    if (human.consort) {
                        addHumanNode(human.consort, level);

                        g.edges.push({
                            id: `${human.id}-${human.consort.id}`,
                            source: human.id,
                            target: human.consort.id,
                            size: 1,
                            color: level == 0 ? '#286090' : '#f5d856'
                        });
                    }

                    // add children only when we going by descendants
                    if (level >= 0) {
                        human.children.forEach(child => addHumanNode(child, level + 1));
                    }
                }

                addHumanNode(root);

                s.graph.read(g);
                s.refresh();
            });

            element.on('$destroy', function() {
                s.graph.clear();
            });
        }
    }
}

greeting.$inject = ['$location'];

export default angular.module('directives.tree', [])
    .directive('sigmajs', greeting)
    .name;
