import angular from 'angular';
import resource from 'angular-resource';

function familyEditorResourceService($resource, $q) {
    /**
     * @param {string} collection
     */
    function getResourceForCollection(collection) {
        var resource = $resource('https://api.mongolab.com/api/1/databases/family-editor/collections/:collection/:id', {
            apiKey: 'xsnXh1zKtybb6cne-Z536ORbMn7-w_fs',
            collection: collection,
            id: '@_id.$oid'
        }, {
            create: { method: 'POST' },
            save: { method: 'PUT' },
            saveList: { method: 'PUT' }
        });

        /**
         * Return expression for make query to mongo by ids
         * @param {string[]} ids
         * @return {string}
         */
        resource.getQueryByIdsExpression = function(ids) {
            var idsString = ids.filter(id => !!id).map(id => `{ $oid: "${id}" }`).join(',');
            return ids.length === 0 ? '' : `{ _id: {$in : [${idsString}] } }`;
        };

        /**
         * Makes query for mongo documents by ids
         * @param {string[]} ids
         * @param {string[]} fields
         * @promise {object[]}
         */
        resource.queryByIds = function(ids, fields) {
            var queryExpression = this.getQueryByIdsExpression(ids);
            var fieldsExpression = fields.map(field => `"${field}" : 1`).join(',');

            var params = {
                q: queryExpression,
                f: fields.length === 0 ? '' : `{${fieldsExpression}}`
            };

            return this.query(params);
        };

        var originSaveList = resource.saveList;

        /**
         * Saves list of documents
         * @param {object[]} items
         * @promise {object}
         */
        resource.saveList = function(items) {
            if (items.length === 0) {
                return $q.when();
            }

            var query = this.getQueryByIdsExpression(items.map(item => item._id.$oid));
            var params = { q: query };

            return originSaveList.call(this, params, items);
        };

        return resource;
    }

    return getResourceForCollection;
}

familyEditorResourceService.$inject = ['$resource', '$q'];

export default angular.module('services.family-editor-resource', [resource])
    .service('familyEditorResource', familyEditorResourceService)
    .name;
