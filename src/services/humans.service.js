import _ from 'lodash';
import angular from 'angular';
import familyEditorResource from './familyEditorResource.service';

import Human from '../entities/human';
import HumanTreeNode from '../entities/humanTreeNode';

/**
 * @param {object} data
 * @param {?string} data.id
 * @param {?string} data.name
 * @param {?Date}   data.birthday
 * @param {?string} data.motherId
 * @param {?string} data.fatherId
 * @param {?string} data.consortId
 * @param {string[]} data.childrenIds
 * @param {string[]} data.ancestors
 * @return {Human}
 */
function createHumanInstance(data) {
    var human = new Human(data);
    human.$origin = data;
    return human;
}

/**
 * @param {Human} human A new human data state
 * @param {Human} originHuman An old human data state
 * @return {string[]}
 */
function getHumanIdsForUpdate(human, originHuman) {
    var dependIds = _.xor(human.childrenIds, originHuman.childrenIds);
    if (human.motherId !== originHuman.motherId) dependIds.push(human.motherId, originHuman.motherId);
    if (human.fatherId !== originHuman.fatherId) dependIds.push(human.fatherId, originHuman.fatherId);
    if (human.consortId !== originHuman.consortId) dependIds.push(human.consortId, originHuman.consortId);
    return dependIds;
}

/**
 * @param {Human} humanToUpdate
 * @param {Human} humanToUnlink
 */
function unlinkHuman(humanToUpdate, humanToUnlink) {
    if (humanToUpdate.motherId === humanToUnlink.id) {
        humanToUpdate.removeParent(humanToUnlink);
    }

    if (humanToUpdate.fatherId === humanToUnlink.id) {
        humanToUpdate.removeParent(humanToUnlink);
    }

    if (humanToUpdate.consortId === humanToUnlink.id) {
        humanToUpdate.consortId = null;
    }

    humanToUpdate.removeChild(humanToUnlink);
}

/**
 * @param {Human} humanToUpdate
 * @param {Human} humanToLink
 */
function linkHuman(humanToUpdate, humanToLink) {
    if (humanToUpdate.id === humanToLink.motherId) {
        humanToUpdate.addChild(humanToLink);
    }
    if (humanToUpdate.id === humanToLink.fatherId) {
        humanToUpdate.addChild(humanToLink);
    }

    if (humanToUpdate.id === humanToLink.consortId) {
        humanToUpdate.consortId = humanToLink.id;
    }

    if (humanToLink.childrenIds.indexOf(humanToUpdate.id) >= 0) {
        humanToUpdate.setParent(humanToLink);
    }
}

function getDescendantsExpression(ids) {
    let idsString = ids.join(`", "`);
    return `{ ancestors: { $in: ["${idsString}"] } }`;
}

function mergeResourceItems(items, itemsToMerge) {
    let itemsForUpdateIndex = _.indexBy(items, '_id.$oid');

    itemsToMerge.forEach(item => {
        if (!itemsForUpdateIndex.hasOwnProperty(item._id.$oid)) {
            items.push(item);
            return;
        }

        var existingItem = itemsForUpdateIndex[item._id.$oid];
        existingItem.ancestors = item.ancestors;
    });
}

class HumansService {
    constructor(familyEditorResource, $q) {
        this.$q = $q;
        this.resource = familyEditorResource('humans');
    }

    /**
     * @param {string} id
     * @return {Promise.<Human>}
     */
    get(id) {
        return this.resource.get({id: id}).$promise.then(data => {
            return typeof data === 'object' && data.hasOwnProperty('_id')
                ? createHumanInstance(data) : null;
        });
    }

    /**
     * @param {string[]} ids
     * @param {boolean} retrieveFullInfo
     * @return {Promise.<Human[]>}
     */
    getList(ids = [], retrieveFullInfo = false) {
        var fields = !retrieveFullInfo ? [ '_id', 'name' ] : [];

        return this.resource.queryByIds(ids, fields).$promise.then(items => {
            var humans = [];
            if (items.constructor !== Array) return humans;

            items.forEach(data => humans.push(createHumanInstance(data)));
            return humans;
        });
    }

    /**
     * @param {Human} human
     */
    getTree(human) {
        let descendantsExpression = getDescendantsExpression([human.id]);
        let query = `${descendantsExpression}`;

        let idsForQuery = human.ancestors.slice(0);
        if (human.consortId) idsForQuery.push(human.consortId);

        if (idsForQuery.length > 0) {
            let idsExpression = this.resource.getQueryByIdsExpression(idsForQuery);
            query = `{ $or : [ ${idsExpression}, ${descendantsExpression} ] }`;
        }

        return this.resource.query({ q: query }).$promise.then(items => {
            let treeRoot = new HumanTreeNode(human);

            /** @type {HumanTreeNode[]} */
            let humans = items.map(item => new HumanTreeNode(item)).concat([treeRoot]);
            let humansIndex = _.indexBy(humans, 'id');

            humans.forEach(human => {
                human.father = humansIndex[human.fatherId];
                human.mother = humansIndex[human.motherId];
                human.consort = humansIndex[human.consortId];
                human.children = human.childrenIds.map(id => humansIndex[id]).filter(human => !!human);
            });

            return treeRoot;
        });
    }

    /**
     * @param {Human} human
     * @return {Promise.<Human>}
     */
    save(human) {
        var newHumanData;
        var data = this.$convertHumanToResource(human);

        return (human.id ? this.$q.when(data) : data.$create()).then(data => {
            newHumanData = data;
            human.id = data._id.$oid;
            return this.$updateDependencies(human);
        }).then(human => {
            let data = this.$convertHumanToResource(human);
            return createHumanInstance(data);
        });
    }

    /**
     * @param {Human} human
     * @promise {Promise.<Human>}
     */
    remove(human) {
        var data = this.$convertHumanToResource(human);
        return data.$remove().then(() => {
            return this.$updateDependencies(human, true);
        });
    }

    /**
     * @param {Human} human
     * @param {boolean} removeOnly Use true to just remove dependencies for record
     * @return {Promise.<Human>}
     */
    $updateDependencies(human, removeOnly = false) {
        var originHuman = human.$origin ? new Human(human.$origin) : new Human();
        if (removeOnly) {
            [ originHuman, human ] = [ human, new Human() ];
        }

        var dependIds = getHumanIdsForUpdate(human, originHuman);
        if (dependIds.length === 0) {
            return this.$convertHumanToResource(human).$save().then(() => human);
        }

        // remember origin ancestors, because it could be modified in future
        var originAncestors = originHuman.ancestors.slice(0);

        var itemsForUpdate = [];
        return this.getList(dependIds, true).then(dependHumans => {
            dependHumans.forEach(dependHuman => {
                unlinkHuman(dependHuman, originHuman);
                linkHuman(dependHuman, human);

                itemsForUpdate.push(this.$convertHumanToResource(dependHuman));
            });

            if (!removeOnly) {
                itemsForUpdate.push(this.$convertHumanToResource(human));
            }

            originHuman.ancestors = originAncestors;
            return this.$updateDescendantsAncestors(human, originHuman);
        }).then(items => {
            mergeResourceItems(itemsForUpdate, items);

            return this.resource.saveList(itemsForUpdate).$promise;
        }).then(() => human);
    }

    /**
     * @param {Human} human A new human data state
     * @param {Human} originHuman An old human data state
     * @returns {object[]}
     */
    $updateDescendantsAncestors(human, originHuman) {
        var ancestorsToAppend = human.ancestors.concat([human.id]);
        var ancestorsToRemove = originHuman.ancestors.concat([originHuman.id]);

        let childrenIds = _.union(human.childrenIds, originHuman.childrenIds);
        if (childrenIds.length <= 0) return this.$q.when([]);

        let queryByIdsExpression = this.resource.getQueryByIdsExpression(childrenIds);
        let descendantsExpression = getDescendantsExpression(childrenIds);
        let query = `{ $or: [ ${descendantsExpression}, ${queryByIdsExpression} ] }`;

        return this.resource.query({ q: query }).$promise.then(items => {
            var recordsToUpdate = [];

            items.forEach(item => {
                let descendant = new Human(item);

                if (originHuman.childrenIds.indexOf(descendant.id)) {
                    descendant.removeAncestors(ancestorsToRemove);
                }

                if (human.childrenIds.indexOf(descendant.id)) {
                    descendant.addAncestors(ancestorsToAppend);
                }

                recordsToUpdate.push(this.$convertHumanToResource(descendant));
            });

            return recordsToUpdate;
        });
    }

    /**
     * @param {Human} human
     * @return {object}
     */
    $convertHumanToResource(human) {
        var data = new this.resource(human);
        if (data.id) {
            data._id = {$oid: data.id};
        }

        delete data['id'];
        delete data['$origin'];

        return data;
    }

    static HumansServiceFactory(familyEditorResource, $q) {
        return new HumansService(familyEditorResource, $q);
    }
}

HumansService.$inject = ['familyEditorResource', '$q'];

export default angular.module('services.humans', [familyEditorResource])
    .factory('HumansService', HumansService.HumansServiceFactory)
    .name;
