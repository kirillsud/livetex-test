import './styles.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './routes';
import humansService from '../../services/humans.service';
import RemoveHumanController from './controller';

export default angular.module('app.humans.remove', [uirouter, humansService])
  .config(routing)
  .controller('RemoveHumanController', RemoveHumanController)
  .name;
