routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    $stateProvider
        .state('removeHuman', {
            url: '/humans/remove/:humanId',
            template: require('./template.html'),
            controller: 'RemoveHumanController',
            controllerAs: 'removeHuman'
        });
}