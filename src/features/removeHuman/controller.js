export default class RemoveHumanController {
    constructor(humansService, $location, $stateParams) {
        this.$location = $location;
        this.$stateParams = $stateParams;
        this.$humans = humansService;

        this.$initHumanModel();
    }

    $initHumanModel() {
        this.$humans.get(this.$stateParams.humanId).then(human => {
            this.human = human;
        }, error => this.error = error);
    }

    remove() {
        this.$humans.remove(this.human).then(() => {
            this.$location.path('/');
        }, error => this.error = error);
    }
}

RemoveHumanController.$inject = ['HumansService', '$location', '$stateParams'];
