import './styles.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './routes';
import humansService from '../../services/humans.service';
import treeDirective from '../../directives/tree.directive';
import UpdateHumanController from './controller';

export default angular.module('app.humans.update', [uirouter, humansService, treeDirective])
  .config(routing)
  .controller('UpdateHumanController', UpdateHumanController)
  .name;
