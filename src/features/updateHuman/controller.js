import _ from 'lodash';
import Human from '../../entities/human';

function isCurrentHuman(human) {
    return this.human ? human.id === this.human.id : false;
}

export default class UpdateHumanController {
    constructor(humansService, $location, $stateParams, $timeout) {
        this.$location = $location;
        this.$stateParams = $stateParams;
        this.$humans = humansService;
        this.$timeout = $timeout;

        this.$initHumansCollection();
        this.$initHumanModel();
    }

    $initHumanModel() {
        this.human = new Human();
        if (!this.$stateParams.hasOwnProperty('humanId')) return;

        this.$humans.get(this.$stateParams.humanId).then(human => {
            this.human = human;

            if (!this.humans) return;
            delete this.humans[this.human.id];

            this.$updateTree();
        }, error => this.error = error);
    }

    $initHumansCollection() {
        this.humans = {};

        this.$humans.getList().then(humans => {
            this.humans = {
                '' : ''
            };
            humans.forEach(human => {
                if (isCurrentHuman.call(this, human)) return;
                this.humans[human.id] = human.name;
            });
        });
    }

    $updateTree() {
        this.$humans.getTree(this.human).then(root => {
            this.treeRoot = root;
        });
    }

    save() {
        this.error = null;
        this.$humans.save(this.human).then((human) => {
            this.human = human;
            this.$location.path(`/humans/edit/${human.id}`);
            this.success = true;
            this.$updateTree();
            this.$timeout(() => this.success = false, 3000);
        }, error => {
            this.error = error;
        });
    }
}

UpdateHumanController.$inject = ['HumansService', '$location', '$stateParams', '$timeout', '$element'];
