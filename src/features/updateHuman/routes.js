routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    $stateProvider
        .state('addHuman', {
            url: '/humans/add',
            template: require('./template.html'),
            controller: 'UpdateHumanController',
            controllerAs: 'updateHuman'
        })
        .state('editHuman', {
            url: '/humans/edit/:humanId',
            template: require('./template.html'),
            controller: 'UpdateHumanController',
            controllerAs: 'updateHuman'
        });
}
