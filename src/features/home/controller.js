import _ from 'lodash';
import Human from '../../entities/human';

export default class HomeController {
    constructor(humansService) {
        this.$humans = humansService;

        this.humansIndex = {};

        this.$humans.getList([], true).then(humans => {
            this.humans = humans;
            this.humansIndex = _.indexBy(humans, 'id');
        });
    }

    /**
     * @param {string} id
     */
    getHumanById(id) {
        return this.humansIndex.hasOwnProperty(id)
            ? this.humansIndex[id] : null;
    }
}

HomeController.$inject = ['HumansService'];
