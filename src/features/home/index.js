import './styles.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './routes';
import HomeController from './controller';
import humansService from '../../services/humans.service';

export default angular.module('app.home', [uirouter, humansService])
  .config(routing)
  .controller('HomeController', HomeController)
  .name;
