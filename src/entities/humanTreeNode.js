import Human from './human';

/**
 * @class HumanTreeNode
 */
export default class HumanTreeNode extends Human {
    /**
     * @param {?object|Human} data
     * @param {?string} data.id
     * @param {?string} data.name
     * @param {?Date}   data.birthday
     * @param {?string} data.motherId
     * @param {?string} data.fatherId
     * @param {?string} data.consortId
     * @param {string[]} data.childrenIds
     * @param {string[]} data.ancestors
     * @constructs Human
     */
    constructor(data = null) {
        super(data);

        /** @member {?HumanTreeNode} Human#father */
        this.father = null;

        /** @member {?HumanTreeNode} Human#mother */
        this.mother = null;

        /** @member {?HumanTreeNode} Human#consort */
        this.consort = null;

        /** @member {HumanTreeNode[]} Human#children */
        this.children = [];
    }
}