import _ from 'lodash';

/**
 * @class Human
 */
export default class Human {
    /**
     * @param {?object|Human} data
     * @param {?string} data.id
     * @param {?string} data.name
     * @param {?Date}   data.birthday
     * @param {?string} data.motherId
     * @param {?string} data.fatherId
     * @param {?string} data.consortId
     * @param {string[]} data.childrenIds
     * @param {string[]} data.ancestors
     * @constructs Human
     */
    constructor(data = null) {
        data = data || {};

        if (data instanceof Human) {
            data = _.assign({ _id: { $oid: data.id } }, data);
        }

        /** @member {?object} Human#id */
        this.id = data._id ? data._id.$oid : null;

        /** @member {?string} Human#name */
        this.name = data.name || null;

        /** @member {?string} Human#gender*/
        this.gender = data.gender || null;

        /** @member {?Date} Human#birthday */
        this.birthday = data.birthday ? new Date(data.birthday) : null;

        /** @member {?string} Human#consortId */
        this.consortId = data.consortId || null;

        /** @member {?string} Human#fatherId */
        this.fatherId = data.fatherId || null;

        /** @member {?string} Human#motherId */
        this.motherId = data.motherId || null;

        /** @member {string[]} Human#childrenIds */
        this.childrenIds = data.childrenIds || [];

        /** @member {string[]} Human#ancestors */
        this.ancestors = data.ancestors || [];
    }

    /**
     * @method setParent
     * @param {Human} human
     */
    setParent(human) {
        if (human.gender === 'male') {
            this.fatherId = human.id;
        } else {
            this.motherId = human.id;
        }

        this.addAncestors(human);
    }

    /**
     * @method removeParent
     * @param {Human} human
     */
    removeParent(human) {
        if (this.fatherId === human.id) {
            this.fatherId = null;
        } else
        if (this.motherId === human.id) {
            this.motherId = null;
        } else {
            return;
        }

        this.removeAncestors(human);
    }

    /**
     * @method addChild
     * @param {Human} human
     */
    addChild(human) {
        if (this.childrenIds.indexOf(human.id) >= 0) return;
        this.childrenIds.push(human.id);
        human.addAncestors(this);
    }

    /**
     * @method removeChild
     * @param {Human} human
     */
    removeChild(human) {
        var index = this.childrenIds.indexOf(human.id);
        if (index < 0) return;
        this.childrenIds.splice(index, 1);
        human.removeAncestors(this);
    }

    /**
     * @param {Human|string[]} human
     */
    addAncestors(human) {
        var ancestors = human.constructor === Array
            ? human : human.ancestors.slice(0).concat([human.id]);

        this.ancestors = _.union(this.ancestors, ancestors);
    }

    /**
     * @param {Human|string[]} human
     */
    removeAncestors(human) {
        var ancestors = human.constructor === Array
            ? human : human.ancestors.slice(0).concat([human.id]);

        this.ancestors = _.without.apply(null, [this.ancestors].concat(ancestors));
    }
}