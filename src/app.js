import 'bootstrap/dist/css/bootstrap.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './app.config';

// features
import home from './features/home';
import updateHuman from './features/updateHuman';
import removeHuman from './features/removeHuman';

angular.module('app', [uirouter, home, updateHuman, removeHuman])
  .config(routing);
